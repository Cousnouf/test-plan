package ch.digity.test_plan;

import ch.digity.test_plan.data.enumeration.NumberFormat;
import ch.digity.test_plan.writer.TestExcelWriter;
import ch.digity.test_plan.writer.TestExcelWriterFactory;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.Clock;
import java.time.LocalDate;

public class LocalReportingExecutor {

    public static void main(String[] args) throws IOException {
        TestExcelWriterFactory factory = new TestExcelWriterFactory(Clock.systemDefaultZone());
        TestExcelWriter testExcelWriter = factory.from(new FileInputStream("E:\\Downloads\\ZFJ-Executions-08-22-2022.csv"),
                LocalDate.of(2022, 8, 15),
                LocalDate.of(2022, 8, 26),
                NumberFormat.PERCENTAGE);

        testExcelWriter.writeWorkbook();
        testExcelWriter.writeToOutputStream(new FileOutputStream("E:/Downloads/test-plan/" + factory.getFileSimpleName("result")));
    }
}