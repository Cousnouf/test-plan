package ch.digity.test_plan.data.tabs.progression;

import ch.digity.test_plan.data.enumeration.ExecutionStatus;
import ch.digity.test_plan.data.enumeration.NumberFormat;
import ch.digity.test_plan.data.enumeration.Priority;
import ch.digity.test_plan.data.tabs.ExcelLine;
import ch.digity.test_plan.data.tabs.ExcelTestSheet;
import ch.digity.test_plan.data.tabs.ExecutionStatusTab;
import ch.digity.test_plan.data.test_execution.TestExecution;
import ch.digity.test_plan.data.test_execution.TestExecutionHolder;
import org.apache.commons.collections4.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Progression extends ExecutionStatusTab implements ExcelTestSheet {
    private final TestExecutionHolder testExecutionHolder;
    private final List<Function<TestExecution, Object>> mappers;
    private final List<List<Object>> values;
    private final String label;
    private final List<Priority> priorities;
    private final NumberFormat numberFormat;

    public Progression(TestExecutionHolder testExecutionHolder, List<Function<TestExecution, Object>> mappers, String label, NumberFormat numberFormat) {
        this(testExecutionHolder, mappers, label, List.of(), numberFormat);
    }

    public Progression(TestExecutionHolder testExecutionHolder, List<Function<TestExecution, Object>> mappers, String label, List<Priority> priorities, NumberFormat numberFormat) {
        this.testExecutionHolder = testExecutionHolder;
        this.mappers = mappers;
        this.values = testExecutionHolder.getDistinctNonEmpty(mappers);
        this.label = label;
        this.priorities = priorities;
        this.numberFormat = numberFormat;
    }

    @Override
    public String getSheetName() {
        return "Progression per " + label.toLowerCase();
    }

    @Override
    public List<ExcelLine> getExcelLines() {
        List<ExcelLine> result = new ArrayList<>();

        for (List<Object> combination : values) {
            Map<ExecutionStatus, List<TestExecution>> executionStatusListMap = groupByExecutionStatus(combination);
            result.add(getExcelLine(combination, executionStatusListMap));
        }
        return result.stream()
                .sorted()
                .collect(Collectors.toList());
    }

    private Map<ExecutionStatus, List<TestExecution>> groupByExecutionStatus(List<Object> combination) {
        return getTestExecutions().stream()
                .filter(testExecution -> correspondsTo(testExecution, combination))
                .collect(Collectors.groupingBy(TestExecution::getExecutionStatus));
    }

    private ProgressionExcelLine getExcelLine(List<Object> combination, Map<ExecutionStatus, List<TestExecution>> executionStatusListMap) {
        return new ProgressionExcelLine(
                combination.stream()
                        .map(String::valueOf)
                        .collect(Collectors.joining(" - ")),
                executionStatusListMap,
                numberFormat);
    }

    private List<TestExecution> getTestExecutions() {
        if (priorities.isEmpty()) {
            return testExecutionHolder.getAll();
        }
        return testExecutionHolder.getAll().stream()
                .filter(testExecution -> priorities.contains(testExecution.getPriority()))
                .collect(Collectors.toList());
    }

    private boolean correspondsTo(TestExecution testExecution, List<Object> combination) {
        List<Object> result = mappers.stream()
                .map(mapper -> mapper.apply(testExecution))
                .collect(Collectors.toList());
        return CollectionUtils.isEqualCollection(result, combination);
    }

    @Override
    protected int getRowNumber() {
        return values.size();
    }

    @Override
    public String getCategoryTitle() {
        return label;
    }
}
