package ch.digity.test_plan.data.tabs.progression.per_maison_country

import ch.digity.test_plan.data.enumeration.Country
import ch.digity.test_plan.data.enumeration.ExecutionStatus
import ch.digity.test_plan.data.enumeration.Maison
import ch.digity.test_plan.data.enumeration.NumberFormat
import ch.digity.test_plan.data.tabs.progression.Progression
import ch.digity.test_plan.data.tabs.progression.ProgressionSpec
import ch.digity.test_plan.data.test_execution.TestExecution
import ch.digity.test_plan.data.test_execution.TestExecutionHolder

import java.util.function.Function

import static ch.digity.test_plan.data.enumeration.Country.AUSTRIA
import static ch.digity.test_plan.data.enumeration.Country.BELGIUM
import static ch.digity.test_plan.data.enumeration.ExecutionStatus.*
import static ch.digity.test_plan.data.enumeration.Maison.BEM
import static ch.digity.test_plan.data.enumeration.Maison.CAR
import static org.apache.commons.lang3.ObjectUtils.defaultIfNull

class GlobalProgressionPerMaisonAndCountrySpec extends ProgressionSpec {

    TestExecutionHolder testExecutionHolder = Stub()

    Progression perMaisonAndCountry

    void setup() {
        testExecutionHolder.getDistinctNonEmpty(_ as List<Function>) >> [[CAR, AUSTRIA], [CAR, BELGIUM], [BEM, AUSTRIA], [BEM, BELGIUM]]
        perMaisonAndCountry = new Progression(testExecutionHolder,
                [asFunction(TestExecution::getMaison), asFunction(TestExecution::getCountry)],
                "Maison & cntry", NumberFormat.PERCENTAGE)
    }

    def "should get proper sheet name"() {
        expect:
        perMaisonAndCountry.getSheetName() == "Progression per maison & cntry"
    }

    def "should get the header"() {
        expect:
        perMaisonAndCountry.getHeader().toValueList().collect { it.stringValue } == [
                "Maison & cntry", "PASS", "WIP", "FAIL", "BLOCKED", "UNEXECUTED", "Total"
        ]
    }

    def "should generate the line properly"() {
        given:
        testExecutionHolder.getAll() >> [te(FAIL, CAR, AUSTRIA), te(), te(), te(),
                                         te(PASS, CAR, BELGIUM), te(PASS, CAR, BELGIUM), te(PASS, CAR, BELGIUM), te(PASS, CAR, BELGIUM),
                                         te(FAIL, BEM, AUSTRIA), te(FAIL, BEM, AUSTRIA), te(PASS, BEM, AUSTRIA), te(PASS, BEM, AUSTRIA), te(PASS, BEM, AUSTRIA), te(PASS, BEM, AUSTRIA), te(PASS, BEM, AUSTRIA),
                                         te(PASS, BEM, BELGIUM), te(PASS, BEM, BELGIUM), te(PASS, BEM, BELGIUM), te(PASS, BEM, BELGIUM), te(BLOCKED, BEM, BELGIUM), te(PASS, BEM, BELGIUM)]

        when:
        def excelLines = perMaisonAndCountry.getExcelLines()

        then:
        excelLines.size() == 4
        excelLines[0].toValueList().collect {
            defaultIfNull(it.stringValue, it.numberValue)
        } == ["BEM - AT", 5, 0.0, 2, 0.0, 0.0, 7.0]
        excelLines[1].toValueList().collect {
            defaultIfNull(it.stringValue, it.numberValue)
        } == ["BEM - BE", 5, 0.0, 0.0, 1, 0.0, 6.0]
        excelLines[2].toValueList().collect {
            defaultIfNull(it.stringValue, it.numberValue)
        } == ["CAR - AT", 3, 0.0, 1, 0.0, 0.0, 4.0]
        excelLines[3].toValueList().collect {
            defaultIfNull(it.stringValue, it.numberValue)
        } == ["CAR - BE", 4.0, 0.0, 0.0, 0.0, 0.0, 4.0]
    }

    TestExecution te(ExecutionStatus executionStatus = PASS, Maison maison = CAR, Country country = AUSTRIA) {
        def testExecution = Stub(TestExecution)
        testExecution.getExecutionStatus() >> executionStatus
        testExecution.getMaison() >> maison
        testExecution.getCountry() >> country
        testExecution
    }
}
