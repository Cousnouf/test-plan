package ch.digity.test_plan.data.enumeration;

public enum NumberFormat {
    PERCENTAGE {
        @Override
        public double getValue(double numberValue, double total) {
            return numberValue / total;
        }
    },
    ABSOLUTE {
        @Override
        public double getValue(double numberValue, double total) {
            return numberValue;
        }
    };

    public abstract double getValue(double numberValue, double total);
}
