package ch.digity.test_plan.data.tabs.defects_per_maison;

import ch.digity.test_plan.data.enumeration.Maison;
import ch.digity.test_plan.data.tabs.ExcelLine;
import ch.digity.test_plan.data.tabs.GroupByTab;
import ch.digity.test_plan.data.tabs.Header;
import ch.digity.test_plan.data.test_execution.TestExecution;
import ch.digity.test_plan.data.test_execution.TestExecutionHolder;

import java.util.List;

/**
 * List number of defects per Maison and Feature
 */
public class DefectsPerMaison extends GroupByTab<Maison> {

    public DefectsPerMaison(TestExecutionHolder testExecutionHolder) {
        super(testExecutionHolder, TestExecution::getMaison);
    }

    @Override
    public String getSheetName() {
        return "Defects per maison";
    }

    @Override
    public Header getHeader() {
        return new Header("Maison", "Number of defects");
    }

    @Override
    protected ExcelLine createExcelLine(Maison key, List<TestExecution> value) {
        return new DefectsPerMaisonExcelLine(key, value);
    }
}
