package ch.digity.test_plan.data.test_execution


import spock.lang.Specification

import java.time.LocalDate

import static ch.digity.test_plan.data.enumeration.Country.*
import static ch.digity.test_plan.data.enumeration.Environment.VAL
import static ch.digity.test_plan.data.enumeration.ExecutionStatus.UNEXECUTED
import static ch.digity.test_plan.data.enumeration.Maison.*
import static ch.digity.test_plan.data.enumeration.Priority.CRITICAL
import static ch.digity.test_plan.data.enumeration.TestPhase.KUT1

class TestExecutionHolderSpec extends Specification {

    def te1 = new TestExecution("cycle1", "issueKey1", "test Summary", "label", KUT1, VAL, CAR, AUSTRIA, "version", 1, CRITICAL, UNEXECUTED, "ncpi", LocalDate.of(2021, 9, 15), "Bob")
    def te2_2 = new TestExecution("cycle1", "issueKey1", "test Summary", "label", KUT1, VAL, BEM, CZECHIA, "version", 2, CRITICAL, UNEXECUTED, "ncpi", LocalDate.of(2021, 9, 15), "Bob")
    def te2 = new TestExecution("cycle1", "issueKey1", "test Summary", "label", KUT1, VAL, BEM, CZECHIA, "version", 1, CRITICAL, UNEXECUTED, "ncpi", LocalDate.of(2021, 9, 15), "Bob")
    def te3 = new TestExecution("cycle1", "issueKey2", "test Summary", "label", KUT1, VAL, IWC, DENMARK, "version", 1, CRITICAL, UNEXECUTED, "ncpi", LocalDate.of(2021, 9, 15), "Bob")
    def te4 = new TestExecution("cycle1", "issueKey2", "test Summary", "label", KUT1, VAL, RDU, AUSTRIA, "version", 1, CRITICAL, UNEXECUTED, "ncpi", LocalDate.of(2021, 9, 15), "Bob")

    TestExecutionHolder holder

    void setup() {
        holder = new TestExecutionHolder([te1, te2_2, te2, te3, te4])
    }

    def "should get from issue key and country"() {
        expect:
        holder.getFrom("issueKey1", CZECHIA).get() == te2
    }

    def "should get from issue key, country and maison"() {
        expect:
        holder.getFrom("issueKey2", DENMARK, BEM).isEmpty()
        holder.getFrom("issueKey2", DENMARK, IWC).get() == te3
    }

    def "should get distinct from mapper"() {
        expect:
        holder.getDistinct({ it -> it.getEnvironment() }) == [VAL]
    }
}
