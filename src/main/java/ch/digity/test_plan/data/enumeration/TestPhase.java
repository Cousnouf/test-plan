package ch.digity.test_plan.data.enumeration;

public enum TestPhase {

    KUT, KUT1, KUT2,
    SIT, SIT1, SIT2
}
