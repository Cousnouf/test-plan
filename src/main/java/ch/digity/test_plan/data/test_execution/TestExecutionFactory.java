package ch.digity.test_plan.data.test_execution;

import ch.digity.test_plan.data.enumeration.*;
import ch.digity.test_plan.data.export_row.ExportRow;
import org.apache.commons.lang3.ObjectUtils;

import java.util.List;
import java.util.stream.Collectors;

import static java.util.Optional.ofNullable;

public class TestExecutionFactory {

    private TestExecutionFactory() {
    }

    public static List<TestExecution> from(List<ExportRow> exportRows) {
        return exportRows.stream()
                .map(TestExecutionFactory::from)
                .collect(Collectors.toList());
    }

    static TestExecution from(ExportRow exportRow) {
        String version = exportRow.getVersion();
        return new TestExecution(
                exportRow.getCycleName(),
                exportRow.getIssueKey(),
                exportRow.getTestSummary(),
                exportRow.getLabels(),
                EnumUtils.from(version, TestPhase.values()),
                EnumUtils.from(version, Environment.values()),
                getMaison(version, exportRow.getCycleName()),
                ObjectUtils.defaultIfNull(Country.from(version), Country.from(exportRow.getCycleName())),
                version,
                exportRow.getOrderId(),
                exportRow.getPriority(),
                exportRow.getExecutionStatus(),
                exportRow.getExecutedBy(),
                exportRow.getExecutedOn(),
                exportRow.getAssignedTo()
        );
    }

    private static Maison getMaison(String version, String cycleName) {
        return ofNullable(EnumUtils.from(version, Maison.values()))
                .orElseGet(() -> EnumUtils.from(cycleName, Maison.values()));
    }
}
