package ch.digity.test_plan.data.tabs;

import ch.digity.test_plan.data.test_execution.TestExecution;
import ch.digity.test_plan.data.test_execution.TestExecutionHolder;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * List number of defects per Maison and Feature
 */
public abstract class GroupByTab<K> implements ExcelTestSheet {
    private final TestExecutionHolder testExecutionHolder;
    private final Function<TestExecution, K> groupyByFunction;

    protected GroupByTab(TestExecutionHolder testExecutionHolder, Function<TestExecution, K> groupyByFunction) {
        this.testExecutionHolder = testExecutionHolder;
        this.groupyByFunction = groupyByFunction;
    }

    @Override
    public List<ExcelLine> getExcelLines() {
        Map<K, List<TestExecution>> perMaisonMap = testExecutionHolder.getAll().stream()
                .filter(testExecution -> groupyByFunction.apply(testExecution) != null)
                .collect(Collectors.groupingBy(groupyByFunction));

        return perMaisonMap.entrySet().stream()
                .map(entry -> createExcelLine(entry.getKey(), entry.getValue()))
                .collect(Collectors.toList());
    }

    protected abstract ExcelLine createExcelLine(K key, List<TestExecution> value);
}
