package ch.digity.test_plan.data.enumeration

import spock.lang.Specification

class CountrySpec extends Specification {

    def "should decode properly Country"() {
        expect:
        Country.from(version) == expected

        where:
        version                         | expected
        "JLC Phoenix E2E_SIT_EU"        | Country.EUROPEAN_UNION
        "JLC Phoenix E2E_SIT_JP"        | Country.JAPAN
        "SEE W4 – SIT – VAL – ALS – UK" | Country.UNITED_KINGDOM
    }
}
