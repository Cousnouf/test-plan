package ch.digity.test_plan.data.chart;

import org.apache.poi.xddf.usermodel.PresetColor;

public class ChartPart {
    private final int columnIndex;
    private final String legend;
    private final PresetColor presetColor;

    public ChartPart(int columnIndex, String legend, PresetColor presetColor) {
        this.columnIndex = columnIndex;
        this.legend = legend;
        this.presetColor = presetColor;
    }

    public int getColumnIndex() {
        return columnIndex;
    }

    public String getLegend() {
        return legend;
    }

    public PresetColor getPresetColor() {
        return presetColor;
    }
}
