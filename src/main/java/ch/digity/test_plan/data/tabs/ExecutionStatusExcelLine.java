package ch.digity.test_plan.data.tabs;

import ch.digity.test_plan.data.enumeration.ExecutionStatus;
import ch.digity.test_plan.data.enumeration.NumberFormat;
import ch.digity.test_plan.data.test_execution.TestExecution;
import org.apache.commons.collections4.ListUtils;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public abstract class ExecutionStatusExcelLine implements Comparable<ExecutionStatusExcelLine> {
    private final Map<ExecutionStatus, List<TestExecution>> executionStatusListMap;
    private final String value;
    protected final int total;

    protected ExecutionStatusExcelLine(Map<ExecutionStatus, List<TestExecution>> executionStatusListMap, String value, int total) {
        this.executionStatusListMap = executionStatusListMap;
        this.value = value;
        this.total = total;
    }

    protected List<CellValueWrapper> toValueListWithRowTitle(NumberFormat numberFormat) {
        List<CellValueWrapper> values = new ArrayList<>();
        values.add(new CellValueWrapper(value));
        for (ExecutionStatus executionStatus : ExecutionStatus.forDisplay()) {
            List<TestExecution> testExecutionsForStatus = ListUtils.defaultIfNull(executionStatusListMap.get(executionStatus), List.of());
            values.add(new CellValueWrapper(testExecutionsForStatus.size(), numberFormat));
        }
        values.add(new CellValueWrapper(total, NumberFormat.ABSOLUTE));

        return values;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ExecutionStatusExcelLine that = (ExecutionStatusExcelLine) o;

        return new EqualsBuilder()
                .append(total, that.total)
                .append(executionStatusListMap, that.executionStatusListMap)
                .append(value, that.value)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(executionStatusListMap)
                .append(value)
                .append(total)
                .toHashCode();
    }

    @Override
    public int compareTo(ExecutionStatusExcelLine o) {
        return value.compareTo(o.value);
    }
}
