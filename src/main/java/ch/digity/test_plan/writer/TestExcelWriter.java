package ch.digity.test_plan.writer;

import ch.digity.test_plan.data.tabs.CellValueWrapper;
import ch.digity.test_plan.data.tabs.ExcelLine;
import ch.digity.test_plan.data.tabs.ExcelTestSheet;
import ch.digity.test_plan.data.tabs.Header;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.List;

public class TestExcelWriter {
    private final Workbook workbook;
    private final List<ExcelTestSheet> excelTestSheets;

    public TestExcelWriter(Workbook workbook, ExcelTestSheet... excelTestSheets) {
        this.workbook = workbook;
        this.excelTestSheets = Arrays.asList(excelTestSheets);
    }

    public void writeWorkbook() {
        for (ExcelTestSheet excelTestSheet : excelTestSheets) {
            Sheet sheet = workbook.createSheet(excelTestSheet.getSheetName());
            write(excelTestSheet.getHeader(), sheet);
            write(excelTestSheet.getExcelLines(), sheet);
            excelTestSheet.performPostWritingAction(sheet);
        }
    }

    public void writeToOutputStream(OutputStream outputStream) throws IOException {
        this.workbook.write(outputStream);
    }

    private void write(Header header, Sheet sheet) {
        write(header, sheet.createRow(0));
    }

    private void write(List<ExcelLine> excelLines, Sheet sheet) {
        int rowIndex = 1;
        for (ExcelLine excelLine : excelLines) {
            write(excelLine, sheet.createRow(rowIndex++));
        }
    }

    private void write(ExcelLine excelLine, Row row) {
        int cellIndex = 0;
        for (CellValueWrapper wrapper : excelLine.toValueList()) {
            Cell cell = row.createCell(cellIndex++);
            wrapper.writeInto(cell, excelLine.getTotal());
        }
    }
}
