package ch.digity.test_plan.data.tabs.burn_down_per_day;

import ch.digity.test_plan.data.tabs.CellValueWrapper;
import ch.digity.test_plan.data.tabs.ExcelLine;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class CombinedBurnDownExcelLine implements ExcelLine {

    private final LocalDate localDate;
    private final BurnDownPerDayExcelLine all;
    private final BurnDownPerDayExcelLine criticalBlocker;

    public CombinedBurnDownExcelLine(LocalDate localDate, BurnDownPerDayExcelLine all, BurnDownPerDayExcelLine criticalBlocker) {
        this.localDate = localDate;
        this.all = all;
        this.criticalBlocker = criticalBlocker;
    }

    @Override
    public List<CellValueWrapper> toValueList() {
        List<CellValueWrapper> values = new ArrayList<>();
        values.add(new CellValueWrapper(localDate.toString()));
        values.addAll(all.toValueList());
        values.addAll(criticalBlocker.toValueList());
        return values;
    }
}
