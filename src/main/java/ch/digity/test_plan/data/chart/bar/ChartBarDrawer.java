package ch.digity.test_plan.data.chart.bar;

import ch.digity.test_plan.data.chart.ChartDrawer;
import ch.digity.test_plan.data.chart.ChartPart;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.CellReference;
import org.apache.poi.xddf.usermodel.XDDFColor;
import org.apache.poi.xddf.usermodel.XDDFShapeProperties;
import org.apache.poi.xddf.usermodel.XDDFSolidFillProperties;
import org.apache.poi.xddf.usermodel.chart.AxisCrossBetween;
import org.apache.poi.xddf.usermodel.chart.AxisCrosses;
import org.apache.poi.xddf.usermodel.chart.AxisPosition;
import org.apache.poi.xddf.usermodel.chart.BarDirection;
import org.apache.poi.xddf.usermodel.chart.BarGrouping;
import org.apache.poi.xddf.usermodel.chart.ChartTypes;
import org.apache.poi.xddf.usermodel.chart.XDDFBarChartData;
import org.apache.poi.xddf.usermodel.chart.XDDFCategoryAxis;
import org.apache.poi.xddf.usermodel.chart.XDDFCategoryDataSource;
import org.apache.poi.xddf.usermodel.chart.XDDFChartData;
import org.apache.poi.xddf.usermodel.chart.XDDFDataSourcesFactory;
import org.apache.poi.xddf.usermodel.chart.XDDFNumericalDataSource;
import org.apache.poi.xddf.usermodel.chart.XDDFValueAxis;
import org.apache.poi.xssf.usermodel.XSSFChart;
import org.apache.poi.xssf.usermodel.XSSFSheet;

import java.util.List;

public class ChartBarDrawer extends ChartDrawer {

    private final XSSFSheet xssfSheet;
    private final int rowNumber;
    private final XSSFChart chart;
    private final XDDFCategoryDataSource categoryDataSource;
    private final XDDFBarChartData data;

    private ChartBarDrawer(XSSFSheet xssfSheet, int rowNumber, XSSFChart chart, XDDFCategoryDataSource categoryDataSource, XDDFBarChartData data) {
        this.xssfSheet = xssfSheet;
        this.rowNumber = rowNumber;
        this.chart = chart;
        this.categoryDataSource = categoryDataSource;
        this.data = data;
    }

    public void traceBars(List<ChartPart> chartParts) {
        chartParts.forEach(this::traceBar);
        data.setBarDirection(BarDirection.BAR);
        data.setBarGrouping(BarGrouping.PERCENT_STACKED);
        chart.plot(data);
    }

    private void traceBar(ChartPart chartPart) {
        int columnIndex = chartPart.getColumnIndex();
        XDDFNumericalDataSource<Double> dataSource = XDDFDataSourcesFactory.fromNumericCellRange(xssfSheet, new CellRangeAddress(1, rowNumber, columnIndex, columnIndex));
        XDDFChartData.Series series = data.addSeries(categoryDataSource, dataSource);
        series.setTitle(chartPart.getLegend(), new CellReference(xssfSheet.getSheetName(), 0, columnIndex, true, true));
        XDDFShapeProperties properties = new XDDFShapeProperties();
        properties.setFillProperties(new XDDFSolidFillProperties(XDDFColor.from(chartPart.getPresetColor())));
        series.setShapeProperties(properties);
    }

    public static ChartBarDrawer of(XSSFSheet xssfSheet, int rowNumber, int leftYPosition, int chartWidthColumn, int chartHeightColumn) {
        XSSFChart chart = getXssfChart(xssfSheet, leftYPosition, chartWidthColumn, chartHeightColumn);

        XDDFCategoryAxis bottomAxis = chart.createCategoryAxis(AxisPosition.BOTTOM);
        XDDFValueAxis leftAxis = chart.createValueAxis(AxisPosition.LEFT);
        leftAxis.setCrosses(AxisCrosses.AUTO_ZERO);
        leftAxis.setCrossBetween(AxisCrossBetween.BETWEEN);

        XDDFCategoryDataSource categoryDataSource = XDDFDataSourcesFactory.fromStringCellRange(xssfSheet, new CellRangeAddress(1, rowNumber, 0, 0));
        XDDFBarChartData data = (XDDFBarChartData) chart.createData(ChartTypes.BAR, bottomAxis, leftAxis);

        return new ChartBarDrawer(xssfSheet, rowNumber, chart, categoryDataSource, data);
    }

}
