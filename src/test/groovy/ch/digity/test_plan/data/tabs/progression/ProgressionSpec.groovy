package ch.digity.test_plan.data.tabs.progression

import ch.digity.test_plan.data.test_execution.TestExecution
import spock.lang.Specification

import java.util.function.Function

class ProgressionSpec extends Specification {

    static Function<TestExecution, Object> asFunction(Closure<Object> closure) {
        new Function<TestExecution, Object>() {
            @Override
            Object apply(TestExecution testExecution) {
                return closure.call(testExecution)
            }
        }
    }
}
