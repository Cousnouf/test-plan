package ch.digity.test_plan.data.tabs.progression.per_maison

import ch.digity.test_plan.data.enumeration.ExecutionStatus
import ch.digity.test_plan.data.enumeration.Maison
import ch.digity.test_plan.data.enumeration.NumberFormat
import ch.digity.test_plan.data.enumeration.Priority
import ch.digity.test_plan.data.tabs.progression.Progression
import ch.digity.test_plan.data.tabs.progression.ProgressionSpec
import ch.digity.test_plan.data.test_execution.TestExecution
import ch.digity.test_plan.data.test_execution.TestExecutionHolder

import java.util.function.Function

import static ch.digity.test_plan.data.enumeration.ExecutionStatus.*
import static ch.digity.test_plan.data.enumeration.Maison.BEM
import static ch.digity.test_plan.data.enumeration.Maison.CAR
import static org.apache.commons.lang3.ObjectUtils.defaultIfNull

class GlobalProgressionPerMaisonSpec extends ProgressionSpec {

    TestExecutionHolder testExecutionHolder = Stub()

    Progression perMaison

    void setup() {
        testExecutionHolder.getDistinctNonEmpty(_ as List<Function>) >> [[CAR], [BEM]]
        testExecutionHolder.getAll() >> [te(FAIL, CAR), te(), te(), te(), te(FAIL), te(), te(),
                                         te(PASS, BEM), te(PASS, BEM), te(PASS, BEM), te(PASS, BEM), te(BLOCKED, BEM), te(PASS, BEM)]
        perMaison = new Progression(testExecutionHolder, [asFunction(TestExecution::getMaison)], "Maison", NumberFormat.PERCENTAGE)
    }

    def "should get proper sheet name"() {
        expect:
        perMaison.getSheetName() == "Progression per maison"
    }

    def "should get the header"() {
        expect:
        perMaison.getHeader().toValueList().collect { it.stringValue } == [
                "Maison", "PASS", "WIP", "FAIL", "BLOCKED", "UNEXECUTED", "Total"
        ]
    }

    def "should generate the line properly"() {
        when:
        def excelLines = perMaison.getExcelLines()

        then:
        excelLines.size() == 2
        excelLines[0].toValueList().collect {
            defaultIfNull(it.stringValue, it.numberValue)
        } == ["BEM", 5, 0, 0, 1, 0, 6]
        excelLines[1].toValueList().collect {
            defaultIfNull(it.stringValue, it.numberValue)
        } == ["CAR", 5, 0, 2, 0, 0, 7]
    }

    TestExecution te(ExecutionStatus executionStatus = PASS, Maison maison = CAR) {
        def testExecution = Stub(TestExecution)
        testExecution.getExecutionStatus() >> executionStatus
        testExecution.getMaison() >> maison
        testExecution.getPriority() >> Priority.MAJOR
        testExecution
    }
}
