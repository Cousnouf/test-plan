package ch.digity.test_plan.data.tabs.front_test_plan;

import ch.digity.test_plan.data.enumeration.Country;
import ch.digity.test_plan.data.enumeration.ExecutionStatus;
import ch.digity.test_plan.data.enumeration.Maison;
import ch.digity.test_plan.data.tabs.ExcelLine;
import ch.digity.test_plan.data.tabs.ExcelTestSheet;
import ch.digity.test_plan.data.tabs.Header;
import ch.digity.test_plan.data.test_execution.TestExecution;
import ch.digity.test_plan.data.test_execution.TestExecutionHolder;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static java.util.Arrays.stream;
import static java.util.stream.Collectors.toMap;

/**
 * Cycle name, issue key, test summary, priority, country
 * Display all maisons and for each line check if it applies (ExecutionStatus)
 * <p>
 * Issue Key -> Country -> Maison
 */
public class FrontTestPlan implements ExcelTestSheet {
    private final TestExecutionHolder testExecutionHolder;

    public FrontTestPlan(TestExecutionHolder testExecutionHolder) {
        this.testExecutionHolder = testExecutionHolder;
    }

    @Override
    public String getSheetName() {
        return "Front test plan";
    }

    @Override
    public Header getHeader() {
        List<String> columnTitles = new ArrayList<>(List.of(
                "Front Issue Key",
                "Test Summary",
                "Priority",
//                "Cycle Name",
                "Country"
        ));
        columnTitles.addAll(Maison.toHeaderValues());
        return new Header(columnTitles);
    }

    @Override
    public List<ExcelLine> getExcelLines() {
        List<String> issueKeys = testExecutionHolder.getDistinct(TestExecution::getIssueKey);
        List<Country> countries = testExecutionHolder.getDistinct(TestExecution::getCountry);

        return issueKeys.stream()
                .flatMap(issueKey -> listForIssueKey(issueKey, countries).stream())
                .collect(Collectors.toList());
    }

    private List<ExcelLine> listForIssueKey(String issueKey, List<Country> countries) {
        return countries.stream()
                .flatMap(country -> testExecutionHolder.getFrom(issueKey, country).stream())
                .map(this::produceLine)
                .collect(Collectors.toList());
    }

    private ExcelLine produceLine(TestExecution testExecution) {
        Map<Maison, ExecutionStatus> maisonExecutionStatusMap = stream(Maison.values())
                .collect(toMap(
                        maison -> maison,
                        maison -> testExecutionHolder.getFrom(testExecution.getIssueKey(), testExecution.getCountry(), maison)
                                .map(TestExecution::getExecutionStatus)
                                .orElse(ExecutionStatus.NA)));

        return FrontTestPlanExcelLine.of(testExecution.getIssueKey(), testExecution.getTestSummary(),
                testExecution.getPriority(), testExecution.getCountry(), maisonExecutionStatusMap);
    }
}
