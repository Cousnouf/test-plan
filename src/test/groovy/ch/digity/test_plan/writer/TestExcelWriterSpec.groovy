package ch.digity.test_plan.writer

import ch.digity.test_plan.data.enumeration.NumberFormat
import ch.digity.test_plan.data.tabs.CellValueWrapper
import ch.digity.test_plan.data.tabs.ExcelLine
import ch.digity.test_plan.data.tabs.ExcelTestSheet
import ch.digity.test_plan.data.tabs.Header
import org.apache.poi.ss.usermodel.Cell
import org.apache.poi.ss.usermodel.Row
import org.apache.poi.ss.usermodel.Sheet
import org.apache.poi.ss.usermodel.Workbook
import spock.lang.Specification

class TestExcelWriterSpec extends Specification {

    Header headerStub = Stub(Header) {
        toValueList() >> [new CellValueWrapper("h1"), new CellValueWrapper("h2"), new CellValueWrapper("h3")]
    }
    ExcelLine excelLine = Stub(ExcelLine) {
        toValueList() >> [new CellValueWrapper("v1"), new CellValueWrapper(0.5, NumberFormat.ABSOLUTE), new CellValueWrapper(0.3, NumberFormat.ABSOLUTE)]
    }
    ExcelTestSheet sheet1 = Stub(ExcelTestSheet) {
        getSheetName() >> "sheetName"
        getHeader() >> headerStub
        getExcelLines() >> [excelLine]
    }
    Workbook workbook = Mock()
    Sheet sheet = Mock()

    TestExcelWriter testExcelWriter

    void setup() {
        testExcelWriter = new TestExcelWriter(workbook, sheet1)
    }

    def "should write excel file"() {
        given:
        Cell cellMock = Mock()
        Row headerRow = Stub(Row) {
            createCell(_) >> cellMock
        }
        Row lineRow = Stub(Row) {
            createCell(_) >> cellMock
        }
        OutputStream outputStream = Mock()

        when:
        testExcelWriter.writeWorkbook()
        testExcelWriter.writeToOutputStream(outputStream)

        then:
        1 * workbook.createSheet("sheetName") >> sheet
        1 * sheet.createRow(0) >> headerRow
        1 * cellMock.setCellValue("h1")
        1 * cellMock.setCellValue("h2")
        1 * cellMock.setCellValue("h3")

        then:
        1 * sheet.createRow(1) >> lineRow
        1 * cellMock.setCellValue("v1")
        1 * cellMock.setCellValue(0.5)
        1 * cellMock.setCellValue(0.3)

        then:
        1 * workbook.write(outputStream)
    }
}
