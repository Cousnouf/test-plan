package ch.digity.test_plan.data.enumeration

import spock.lang.Specification
import spock.lang.Unroll

class NumberFormatSpec extends Specification {

    @Unroll
    def "should calculate percentage"() {
        expect:
        NumberFormat.PERCENTAGE.getValue(numberValue, total) == expected

        where:
        numberValue | total | expected
        45          | 60    | 0.75
        45          | 450   | 0.10
        432         | 432   | 1
        125         | 34287 | 0.0036456966197100944
        1           | 200   | 0.005
    }
}
