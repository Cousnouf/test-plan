package ch.digity.test_plan.data.test_execution

import spock.lang.Specification

class TestExecutionSpec extends Specification {

    def "should check the first step"() {
        given:
        def testExecutionBuilder = TestExecution.builder().orderId(orderId).build()

        expect:
        testExecutionBuilder.isFirstStep() == expected

        where:
        orderId | expected
        1       | true
        null    | true
        2       | false
        256     | false
    }
}
