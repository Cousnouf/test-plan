package ch.digity.test_plan.data.tabs.defects_per_maison;

import ch.digity.test_plan.data.enumeration.Maison;
import ch.digity.test_plan.data.enumeration.NumberFormat;
import ch.digity.test_plan.data.tabs.CellValueWrapper;
import ch.digity.test_plan.data.tabs.ExcelLine;
import ch.digity.test_plan.data.test_execution.TestExecution;

import java.util.List;

public class DefectsPerMaisonExcelLine implements ExcelLine {
    private final Maison maison;
    private final List<TestExecution> testExecutions;

    public DefectsPerMaisonExcelLine(Maison maison, List<TestExecution> testExecutions) {
        this.maison = maison;
        this.testExecutions = testExecutions;
    }

    @Override
    public List<CellValueWrapper> toValueList() {
        return List.of(
                new CellValueWrapper(maison.name()),
                new CellValueWrapper(testExecutions.size(), NumberFormat.ABSOLUTE)
        );
    }
}
