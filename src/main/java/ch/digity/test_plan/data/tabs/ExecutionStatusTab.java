package ch.digity.test_plan.data.tabs;

import ch.digity.test_plan.data.chart.ChartPart;
import ch.digity.test_plan.data.chart.bar.ChartBarDrawer;
import ch.digity.test_plan.data.enumeration.ExecutionStatus;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xddf.usermodel.PresetColor;
import org.apache.poi.xssf.usermodel.XSSFSheet;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public abstract class ExecutionStatusTab implements ExcelTestSheet {

    public Header getHeader() {
        List<String> columnTitles = new ArrayList<>();
        columnTitles.add(getCategoryTitle());
        columnTitles.addAll(ExecutionStatus.forDisplay().stream()
                .map(Enum::toString).collect(Collectors.toList()));
        columnTitles.add("Total");
        return new Header(columnTitles);
    }

    @Override
    public void performPostWritingAction(Sheet sheet) {
        if (getRowNumber() == 0) {
            return;
        }
        ChartBarDrawer chartBarDrawer = ChartBarDrawer.of((XSSFSheet) sheet, getRowNumber(), getRowNumber() + 2, 15, 35);
        chartBarDrawer.traceBars(List.of(
                new ChartPart(1, "PASS", PresetColor.GREEN),
                new ChartPart(2, "WIP", PresetColor.ORANGE),
                new ChartPart(3, "FAIL", PresetColor.INDIAN_RED),
                new ChartPart(4, "BLOCKED", PresetColor.CORNFLOWER_BLUE),
                new ChartPart(5, "UNEXECUTED", PresetColor.LIGHT_GRAY)
        ));
    }

    protected abstract int getRowNumber();

    protected abstract String getCategoryTitle();
}
