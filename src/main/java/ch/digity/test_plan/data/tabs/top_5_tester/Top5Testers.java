package ch.digity.test_plan.data.tabs.top_5_tester;

import ch.digity.test_plan.data.tabs.ExcelLine;
import ch.digity.test_plan.data.tabs.GroupByTab;
import ch.digity.test_plan.data.tabs.Header;
import ch.digity.test_plan.data.test_execution.TestExecution;
import ch.digity.test_plan.data.test_execution.TestExecutionHolder;

import java.util.List;

/**
 * Count by executedBy
 */
public class Top5Testers extends GroupByTab<String> {

    public Top5Testers(TestExecutionHolder testExecutionHolder) {
        super(testExecutionHolder, TestExecution::getExecutedBy);
    }

    @Override
    public String getSheetName() {
        return "Top 5 testers";
    }

    @Override
    public Header getHeader() {
        return new Header("Tester", "Number of cases tested");
    }

    @Override
    protected ExcelLine createExcelLine(String key, List<TestExecution> value) {
        return new Top5TestersExcelLine(key, value.size());
    }
}
