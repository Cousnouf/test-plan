package ch.digity.test_plan.data.tabs.progression

import ch.digity.test_plan.data.enumeration.NumberFormat
import spock.lang.Specification

class ProgressionExcelLineSpec extends Specification {
    ProgressionExcelLine constant = new ProgressionExcelLine("a", Map.of(), NumberFormat.ABSOLUTE)

    def "Equals"() {
        given:
        def a = new ProgressionExcelLine(value, Map.of(), numberFormat)

        expect:
        a.equals(constant) == expected

        where:
        value | numberFormat            | expected
        "a"   | NumberFormat.ABSOLUTE   | true
        "a"   | NumberFormat.PERCENTAGE | false
        "b"   | NumberFormat.ABSOLUTE   | false
    }

    def "Not equals"() {
        expect:
        !constant.equals(new StringIndexOutOfBoundsException())
        constant.equals(constant)
        !constant.equals(null)
    }

    def "HashCode"() {
        given:
        def a = new ProgressionExcelLine(value, Map.of(), numberFormat)

        expect:
        (a.hashCode() == constant.hashCode()) == expected

        where:
        value | numberFormat            | expected
        "a"   | NumberFormat.ABSOLUTE   | true
        "a"   | NumberFormat.PERCENTAGE | false
        "b"   | NumberFormat.ABSOLUTE   | false
    }
}
