package ch.digity.test_plan.data.tabs.progression.per_maison_blocker_critical

import ch.digity.test_plan.data.enumeration.ExecutionStatus
import ch.digity.test_plan.data.enumeration.Maison
import ch.digity.test_plan.data.enumeration.NumberFormat
import ch.digity.test_plan.data.enumeration.Priority
import ch.digity.test_plan.data.tabs.progression.Progression
import ch.digity.test_plan.data.tabs.progression.ProgressionSpec
import ch.digity.test_plan.data.test_execution.TestExecution
import ch.digity.test_plan.data.test_execution.TestExecutionHolder

import java.util.function.Function

import static ch.digity.test_plan.data.enumeration.ExecutionStatus.*
import static ch.digity.test_plan.data.enumeration.Maison.BEM
import static ch.digity.test_plan.data.enumeration.Maison.CAR
import static ch.digity.test_plan.data.enumeration.Priority.*
import static org.apache.commons.lang3.ObjectUtils.defaultIfNull

class GlobalProgressionPerMaisonBlockerCriticalSpec extends ProgressionSpec {

    TestExecutionHolder testExecutionHolder = Stub()

    Progression perMaisonBlockerCritical

    void setup() {
        testExecutionHolder.getDistinctNonEmpty(_ as List<Function>) >> [[CAR], [BEM]]
        perMaisonBlockerCritical = new Progression(testExecutionHolder, [asFunction(TestExecution::getMaison)], "Maison (bl, cri)", [BLOCKER, CRITICAL], NumberFormat.ABSOLUTE)
    }

    def "should get proper sheet name"() {
        expect:
        perMaisonBlockerCritical.getSheetName() == "Progression per maison (bl, cri)"
    }

    def "should get the header"() {
        expect:
        perMaisonBlockerCritical.getHeader().toValueList().collect { it.stringValue } == [
                "Maison (bl, cri)", "PASS", "WIP", "FAIL", "BLOCKED", "UNEXECUTED", "Total"
        ]
    }

    def "should generate the line properly"() {
        given:
        testExecutionHolder.getAll() >> [te(FAIL, CAR, CRITICAL), te(), te(), te(), te(FAIL), te(),
                                         te(PASS, CAR, CRITICAL), te(PASS, CAR, MAJOR), te(PASS, CAR, MAJOR),
                                         te(PASS, BEM, CRITICAL), te(PASS, BEM, CRITICAL), te(PASS, BEM, CRITICAL),
                                         te(PASS, BEM, CRITICAL), te(BLOCKED, BEM, CRITICAL), te(PASS, BEM, CRITICAL), te(PASS, BEM, MAJOR)]

        when:
        def excelLines = perMaisonBlockerCritical.getExcelLines()

        then:
        excelLines.size() == 2
        excelLines[0].toValueList().collect {
            defaultIfNull(it.stringValue, it.numberValue)
        } == ["BEM", 5, 0.0, 0.0, 1, 0.0, 6.0]
        excelLines[1].toValueList().collect {
            defaultIfNull(it.stringValue, it.numberValue)
        } == ["CAR", 5, 0.0, 2, 0.0, 0.0, 7.0]
    }

    TestExecution te(ExecutionStatus executionStatus = PASS, Maison maison = CAR, Priority priority = BLOCKER) {
        def testExecution = Stub(TestExecution)
        testExecution.getExecutionStatus() >> executionStatus
        testExecution.getMaison() >> maison
        testExecution.getPriority() >> priority
        testExecution.isCriticalOrBlocker() >> (priority == BLOCKER || priority == CRITICAL)
        testExecution
    }
}
