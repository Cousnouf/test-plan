package ch.digity.test_plan.data.tabs.top_5_tester;

import ch.digity.test_plan.data.enumeration.NumberFormat;
import ch.digity.test_plan.data.tabs.CellValueWrapper;
import ch.digity.test_plan.data.tabs.ExcelLine;

import java.util.List;

public class Top5TestersExcelLine implements ExcelLine {
    private final String tester;
    private final int caseTestedNumber;

    public Top5TestersExcelLine(String tester, int caseTestedNumber) {
        this.tester = tester;
        this.caseTestedNumber = caseTestedNumber;
    }

    @Override
    public List<CellValueWrapper> toValueList() {
        return List.of(
                new CellValueWrapper(tester),
                new CellValueWrapper(caseTestedNumber, NumberFormat.ABSOLUTE)
        );
    }
}
