package ch.digity.test_plan.data.tabs;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Header implements ExcelLine {
    private final List<String> columnTitles;

    public Header(List<String> columnTitles) {
        this.columnTitles = columnTitles;
    }

    public Header(String... headers) {
        this(Arrays.asList(headers));
    }

    @Override
    public List<CellValueWrapper> toValueList() {
        return columnTitles.stream()
                .map(CellValueWrapper::new)
                .collect(Collectors.toList());
    }
}
