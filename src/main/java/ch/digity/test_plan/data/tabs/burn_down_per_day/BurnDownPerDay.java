package ch.digity.test_plan.data.tabs.burn_down_per_day;

import ch.digity.test_plan.data.chart.ChartPart;
import ch.digity.test_plan.data.chart.line.ChartLineDrawer;
import ch.digity.test_plan.data.tabs.ExcelLine;
import ch.digity.test_plan.data.tabs.ExcelTestSheet;
import ch.digity.test_plan.data.tabs.Header;
import ch.digity.test_plan.data.test_execution.TestExecution;
import ch.digity.test_plan.data.test_execution.TestExecutionHolder;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xddf.usermodel.PresetColor;
import org.apache.poi.xssf.usermodel.XSSFSheet;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;
import java.util.stream.LongStream;

/**
 * For each execution day display the number of test cases executed compared to the daily average that should have
 * been done
 */
public class BurnDownPerDay implements ExcelTestSheet {

    private final TestExecutionHolder testExecutionHolder;
    private final LocalDate dateFrom;
    private final LocalDate dateTo;

    private int rowNumber;

    public BurnDownPerDay(TestExecutionHolder testExecutionHolder, LocalDate dateFrom, LocalDate dateTo) {
        this.testExecutionHolder = testExecutionHolder;
        this.dateFrom = dateFrom;
        this.dateTo = dateTo;
    }

    @Override
    public String getSheetName() {
        return "Burn down";
    }

    @Override
    public Header getHeader() {
        return new Header("Date", "Remaining", "Remaining average", "Executed", "Average", "Remaining (Bl/Cr)", "Remaining average (Bl/Cr)", "Executed (Bl/Cr)", "Average (Bl/Cr)");
    }

    @Override
    public List<ExcelLine> getExcelLines() {
        List<BurnDownPerDayExcelLine> all = getExcelLines(testExecutionHolder.getAll());
        List<BurnDownPerDayExcelLine> blockerAndCritical = getExcelLines(testExecutionHolder.getBlockersAndCritical());
        this.rowNumber = all.size();

        List<ExcelLine> combined = new ArrayList<>();

        for (int i = 0; i < all.size(); i++) {
            BurnDownPerDayExcelLine allLine = all.get(i);
            BurnDownPerDayExcelLine blockerCriticalLine = blockerAndCritical.get(i);
            combined.add(new CombinedBurnDownExcelLine(allLine.getLocalDate(),
                    allLine,
                    blockerCriticalLine)
            );
        }

        return combined;
    }

    List<BurnDownPerDayExcelLine> getExcelLines(List<TestExecution> testExecutions) {
        Map<LocalDate, List<TestExecution>> groupedByExecutedOn = getGroupedByExecutedOn(testExecutions);
        List<WorkingDay> workingDays = getWorkingDaysBetweenMinAndMax(dateFrom, dateTo, groupedByExecutedOn);
        int testExecutionSize = testExecutions.size();
        double average = (double) testExecutionSize / (double) workingDays.size();

        int remaining = testExecutionSize;
        double remainingAverage = testExecutionSize;
        List<BurnDownPerDayExcelLine> result = new ArrayList<>();
        for (WorkingDay workingDay : workingDays) {
            int executed = workingDay.getTestExecutions().size();
            remaining -= executed;
            remainingAverage -= average;
            result.add(new BurnDownPerDayExcelLine(workingDay.getLocalDate(), remaining, remainingAverage, executed, average));
        }
        return result;
    }

    private TreeMap<LocalDate, List<TestExecution>> getGroupedByExecutedOn(List<TestExecution> testExecutions) {
        return testExecutions.stream()
                .filter(testExecution -> testExecution.getExecutionStatus().isExecuted())
                .collect(Collectors.groupingBy(TestExecution::getExecutedOn, TreeMap::new, Collectors.toList()));
    }

    @Override
    public void performPostWritingAction(Sheet sheet) {
        XSSFSheet xssfSheet = (XSSFSheet) sheet;

        ChartLineDrawer chartLineDrawer = ChartLineDrawer.of("Burndown", "Cases executed", xssfSheet, rowNumber, rowNumber + 2, 15, 25);
        chartLineDrawer.traceLines(List.of(
                new ChartPart(1, "Executed", PresetColor.CHARTREUSE),
                new ChartPart(2, "Average", PresetColor.TURQUOISE),
                new ChartPart(5, "Executed (Bl/Crit)", PresetColor.ORANGE),
                new ChartPart(6, "Average (Bl/Crit)", PresetColor.INDIAN_RED)
        ));
    }

    private List<WorkingDay> getWorkingDaysBetweenMinAndMax(final LocalDate min, final LocalDate max, Map<LocalDate, List<TestExecution>> groupedByExecutedOn) {
        return LongStream.range(0, ChronoUnit.DAYS.between(min, max) + 1)
                .mapToObj(min::plusDays)
                .map(localDate -> WorkingDay.of(localDate, groupedByExecutedOn))
                .filter(WorkingDay::isWeekDayOrHasBeenWorked)
                .collect(Collectors.toList());
    }
}
