package ch.digity.test_plan.data.enumeration;

public enum Priority {

    BLOCKER,
    CRITICAL,
    MAJOR,
    MINOR,
    TRIVIAL
}
