package ch.digity.test_plan.data.tabs.burn_down_per_day

import ch.digity.test_plan.data.enumeration.ExecutionStatus
import ch.digity.test_plan.data.test_execution.TestExecution
import ch.digity.test_plan.data.test_execution.TestExecutionHolder
import spock.lang.Specification

import static java.time.LocalDate.of
import static org.apache.commons.lang3.ObjectUtils.defaultIfNull

class BurnDownPerDaySpec extends Specification {

    TestExecutionHolder testExecutionHolder = Stub()

    BurnDownPerDay burnDownPerDay

    void setup() {
        burnDownPerDay = new BurnDownPerDay(testExecutionHolder, of(2021, 6, 7), of(2021, 6, 30))
    }

    def "should get proper sheet name"() {
        expect:
        burnDownPerDay.getSheetName() == "Burn down"
    }

    def "should get the header"() {
        expect:
        burnDownPerDay.getHeader().toValueList().collect { it.stringValue } == [
                "Date", "Remaining", "Remaining average", "Executed", "Average",
                "Remaining (Bl/Cr)", "Remaining average (Bl/Cr)", "Executed (Bl/Cr)", "Average (Bl/Cr)"
        ]
    }

    def "should produce ordered and consistent excel lines"() {
        given:
        testExecutionHolder.getAll() >> [
                te(), te(), te(), te(), te(), te(), te(), te(), te(),
                te(8), te(8), te(8), te(8), te(8),
                te(9),
                // nothing done the 10
                te(11), te(11), te(11),
                // 12, 13 = weekend
                te(14), te(14), te(14), te(14), te(14), te(14), te(14), te(14), te(14), te(14), te(14), te(14),
                te(15), te(15), te(15),
                te(16), te(16), te(16), te(16), te(16), te(16), te(16),
                te(17), te(17), te(17),
                te(18), te(18), te(18), te(18), te(18), te(18), te(18), te(18),
                // 19, 20 = weekend
                te(21), te(21), te(21), te(21), te(21), te(21), te(21), te(21),
                te(22), te(22), te(22), te(22), te(22), te(22),
                te(23), te(23), te(23), te(23), te(23),
                te(24), te(24), te(24), te(24), te(24), te(24), te(24), te(24), te(24), te(24), te(24), te(24),
                // nothing done the 25
                // 26, 27 = weekend
                te(28), te(28),
                te(29),
                te(30), te(30), te(30), te(30), te(30),
        ]

        when:
        def excelLines = burnDownPerDay.getExcelLines()

        then:
        excelLines.size() == 18
        excelLines[0].toValueList().collect { defaultIfNull(it.stringValue, it.numberValue) } ==
                ["2021-06-07", 81.0, 85.0, 9.0, 5.0, 0.0, 0.0, 0.0, 0.0]
        excelLines[1].toValueList().collect { defaultIfNull(it.stringValue, it.numberValue) } ==
                ["2021-06-08", 76.0, 80.0, 5.0, 5.0, 0.0, 0.0, 0.0, 0.0]
        excelLines[2].toValueList().collect { defaultIfNull(it.stringValue, it.numberValue) } ==
                ["2021-06-09", 75.0, 75.0, 1.0, 5.0, 0.0, 0.0, 0.0, 0.0]
        excelLines[3].toValueList().collect { defaultIfNull(it.stringValue, it.numberValue) } ==
                ["2021-06-10", 75.0, 70.0, 0.0, 5.0, 0.0, 0.0, 0.0, 0.0]
        excelLines[4].toValueList().collect { defaultIfNull(it.stringValue, it.numberValue) } ==
                ["2021-06-11", 72.0, 65.0, 3.0, 5.0, 0.0, 0.0, 0.0, 0.0]
        excelLines[5].toValueList().collect { defaultIfNull(it.stringValue, it.numberValue) } ==
                ["2021-06-14", 60.0, 60.0, 12.0, 5.0, 0.0, 0.0, 0.0, 0.0]
        excelLines[6].toValueList().collect { defaultIfNull(it.stringValue, it.numberValue) } ==
                ["2021-06-15", 57.0, 55.0, 3.0, 5.0, 0.0, 0.0, 0.0, 0.0]
        excelLines[7].toValueList().collect { defaultIfNull(it.stringValue, it.numberValue) } ==
                ["2021-06-16", 50.0, 50.0, 7.0, 5.0, 0.0, 0.0, 0.0, 0.0]
        excelLines[8].toValueList().collect { defaultIfNull(it.stringValue, it.numberValue) } ==
                ["2021-06-17", 47.0, 45.0, 3.0, 5.0, 0.0, 0.0, 0.0, 0.0]
        excelLines[9].toValueList().collect { defaultIfNull(it.stringValue, it.numberValue) } ==
                ["2021-06-18", 39.0, 40.0, 8.0, 5.0, 0.0, 0.0, 0.0, 0.0]
        excelLines[10].toValueList().collect { defaultIfNull(it.stringValue, it.numberValue) } ==
                ["2021-06-21", 31.0, 35.0, 8.0, 5.0, 0.0, 0.0, 0.0, 0.0]
        excelLines[11].toValueList().collect { defaultIfNull(it.stringValue, it.numberValue) } ==
                ["2021-06-22", 25.0, 30.0, 6.0, 5.0, 0.0, 0.0, 0.0, 0.0]
        excelLines[12].toValueList().collect { defaultIfNull(it.stringValue, it.numberValue) } ==
                ["2021-06-23", 20.0, 25.0, 5.0, 5.0, 0.0, 0.0, 0.0, 0.0]
        excelLines[13].toValueList().collect { defaultIfNull(it.stringValue, it.numberValue) } ==
                ["2021-06-24", 8.0, 20.0, 12.0, 5.0, 0.0, 0.0, 0.0, 0.0]
        excelLines[14].toValueList().collect { defaultIfNull(it.stringValue, it.numberValue) } ==
                ["2021-06-25", 8.0, 15.0, 0.0, 5.0, 0.0, 0.0, 0.0, 0.0]
        excelLines[15].toValueList().collect { defaultIfNull(it.stringValue, it.numberValue) } ==
                ["2021-06-28", 6.0, 10.0, 2.0, 5.0, 0.0, 0.0, 0.0, 0.0]
        excelLines[16].toValueList().collect { defaultIfNull(it.stringValue, it.numberValue) } ==
                ["2021-06-29", 5.0, 5.0, 1.0, 5.0, 0.0, 0.0, 0.0, 0.0]
        excelLines[17].toValueList().collect { defaultIfNull(it.stringValue, it.numberValue) } ==
                ["2021-06-30", 0.0, 0.0, 5.0, 5.0, 0.0, 0.0, 0.0, 0.0]

    }

    TestExecution te(int day = 7, int month = 6, int year = 2021) {
        TestExecution testExecution = Stub(TestExecution) {
            getExecutedOn() >> of(year, month, day)
            getExecutionStatus() >> ExecutionStatus.PASS
        }
        testExecution
    }
}
