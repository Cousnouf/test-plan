package ch.digity.test_plan.writer;

import ch.digity.test_plan.data.enumeration.NumberFormat;
import ch.digity.test_plan.data.enumeration.Priority;
import ch.digity.test_plan.data.export_row.ExportRow;
import ch.digity.test_plan.data.export_row.ExportRowFactory;
import ch.digity.test_plan.data.tabs.burn_down_per_day.BurnDownPerDay;
import ch.digity.test_plan.data.tabs.defects_per_maison.DefectsPerMaison;
import ch.digity.test_plan.data.tabs.front_test_plan.FrontTestPlan;
import ch.digity.test_plan.data.tabs.progression.Progression;
import ch.digity.test_plan.data.tabs.top_5_tester.Top5Testers;
import ch.digity.test_plan.data.test_execution.TestExecution;
import ch.digity.test_plan.data.test_execution.TestExecutionFactory;
import ch.digity.test_plan.data.test_execution.TestExecutionHolder;
import lombok.RequiredArgsConstructor;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.time.Clock;
import java.time.LocalDate;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import static java.util.Comparator.naturalOrder;
import static java.util.Comparator.reverseOrder;

@RequiredArgsConstructor
public class TestExcelWriterFactory {

    private final Clock clock;

    public TestExcelWriter from(InputStream inputStream, LocalDate dateFrom, LocalDate dateTo) {
        return from(inputStream, dateFrom, dateTo, NumberFormat.PERCENTAGE);
    }

    public TestExcelWriter from(InputStream inputStream, LocalDate dateFrom, LocalDate dateTo, NumberFormat progressionNumberFormat) {
        List<ExportRow> exportRows = ExportRowFactory.from(inputStream);
        List<TestExecution> testExecutions = TestExecutionFactory.from(exportRows);

        TestExecutionHolder testExecutionHolder = new TestExecutionHolder(testExecutions);

        return new TestExcelWriter(new XSSFWorkbook(),
                new FrontTestPlan(testExecutionHolder),
                new Progression(testExecutionHolder, List.of(TestExecution::getCycleName), "Cycle name", progressionNumberFormat),
                new Progression(testExecutionHolder, List.of(TestExecution::getLabels), "Labels", progressionNumberFormat),
                new Progression(testExecutionHolder, List.of(TestExecution::getMaison, TestExecution::getCountry), "Maison - Country", progressionNumberFormat),
                new Progression(testExecutionHolder, List.of(TestExecution::getVersion), "Version", progressionNumberFormat),
                new Progression(testExecutionHolder, List.of(TestExecution::getMaison), "Maison", progressionNumberFormat),
                new Progression(testExecutionHolder, List.of(TestExecution::getMaison), "Maison (Block-Crit)", List.of(Priority.BLOCKER, Priority.CRITICAL), progressionNumberFormat),
                new Progression(testExecutionHolder, List.of(TestExecution::getMaison), "Maison (Major)", List.of(Priority.MAJOR), progressionNumberFormat),
                new BurnDownPerDay(testExecutionHolder, getDate(dateFrom, testExecutions, naturalOrder()), getDate(dateTo, testExecutions, reverseOrder())),
                new Top5Testers(testExecutionHolder),
                new DefectsPerMaison(testExecutionHolder)
        );
    }

    private LocalDate getDate(LocalDate localDate, List<TestExecution> testExecutions, Comparator<LocalDate> comparator) {
        if (localDate != null) {
            return localDate;
        }
        return testExecutions.stream()
                .map(TestExecution::getExecutedOn)
                .filter(Objects::nonNull)
                .sorted(comparator)
                .reduce((localDate1, localDate2) -> localDate1)
                .orElse(LocalDate.now(clock));
    }

    public String getFileSimpleName(String simpleName) {
        return new SimpleDateFormat("yyyy-MM-dd_HHmmss").format(new Date(clock.millis())) + "_" + simpleName + ".xlsx";
    }
}
