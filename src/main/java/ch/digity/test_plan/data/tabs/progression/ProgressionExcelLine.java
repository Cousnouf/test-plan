package ch.digity.test_plan.data.tabs.progression;

import ch.digity.test_plan.data.enumeration.ExecutionStatus;
import ch.digity.test_plan.data.enumeration.NumberFormat;
import ch.digity.test_plan.data.tabs.CellValueWrapper;
import ch.digity.test_plan.data.tabs.ExcelLine;
import ch.digity.test_plan.data.tabs.ExecutionStatusExcelLine;
import ch.digity.test_plan.data.test_execution.TestExecution;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.util.List;
import java.util.Map;

public class ProgressionExcelLine extends ExecutionStatusExcelLine implements ExcelLine {

    private final NumberFormat numberFormat;

    public ProgressionExcelLine(String value, Map<ExecutionStatus, List<TestExecution>> executionStatusListMap, NumberFormat numberFormat) {
        super(executionStatusListMap, value, executionStatusListMap.values().stream().mapToInt(List::size).sum());
        this.numberFormat = numberFormat;
    }

    @Override
    public double getTotal() {
        return total;
    }

    @Override
    public List<CellValueWrapper> toValueList() {
        return toValueListWithRowTitle(numberFormat);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ProgressionExcelLine that = (ProgressionExcelLine) o;

        return new EqualsBuilder().appendSuper(super.equals(o)).append(numberFormat, that.numberFormat).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).appendSuper(super.hashCode()).append(numberFormat).toHashCode();
    }
}
