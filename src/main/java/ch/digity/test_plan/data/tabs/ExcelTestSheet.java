package ch.digity.test_plan.data.tabs;

import org.apache.poi.ss.usermodel.Sheet;

import java.util.List;

public interface ExcelTestSheet {

    String getSheetName();

    Header getHeader();

    List<ExcelLine> getExcelLines();

    default void performPostWritingAction(Sheet sheet) {
    }
}
