package ch.digity.test_plan.data.enumeration;

import com.opencsv.bean.AbstractBeanField;

public class PriorityConverter extends AbstractBeanField<Priority, String> {
    @Override
    protected Object convert(String value) {
        return Priority.valueOf(value.toUpperCase());
    }
}
