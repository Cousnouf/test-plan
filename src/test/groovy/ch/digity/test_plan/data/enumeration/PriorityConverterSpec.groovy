package ch.digity.test_plan.data.enumeration

import spock.lang.Specification

class PriorityConverterSpec extends Specification {

    def "should convert"() {
        expect:
        new PriorityConverter().convert(value) == Priority.CRITICAL

        where:
        value      | _
        "critical" | _
        "CRITICAL" | _
        "Critical" | _
    }
}
