package ch.digity.test_plan.data.tabs;

import ch.digity.test_plan.data.enumeration.NumberFormat;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.CellValue;

public class CellValueWrapper {
    private final CellValue cellValue;
    private final NumberFormat numberFormat;

    public CellValueWrapper(String value) {
        this.cellValue = new CellValue(value);
        this.numberFormat = NumberFormat.ABSOLUTE;
    }

    public CellValueWrapper(double value, NumberFormat numberFormat) {
        this.cellValue = new CellValue(value);
        this.numberFormat = numberFormat;
    }

    public CellValueWrapper(int value, NumberFormat numberFormat) {
        this.cellValue = new CellValue(value);
        this.numberFormat = numberFormat;
    }

    public CellValueWrapper(CellValue cellValue, NumberFormat numberFormat) {
        this.cellValue = cellValue;
        this.numberFormat = numberFormat;
    }

    public void writeInto(Cell cell, double total) {
        if (cellValue.getCellType() == CellType.NUMERIC) {
            cell.setCellValue(numberFormat.getValue(cellValue.getNumberValue(), total));
        } else {
            cell.setCellValue(cellValue.getStringValue());
        }
    }

    public String getStringValue() {
        return cellValue.getStringValue();
    }

    public double getNumberValue() {
        return cellValue.getNumberValue();
    }
}
