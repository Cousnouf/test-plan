package ch.digity.test_plan.data.test_execution;

import ch.digity.test_plan.data.enumeration.Country;
import ch.digity.test_plan.data.enumeration.Environment;
import ch.digity.test_plan.data.enumeration.ExecutionStatus;
import ch.digity.test_plan.data.enumeration.Maison;
import ch.digity.test_plan.data.enumeration.Priority;
import ch.digity.test_plan.data.enumeration.TestPhase;
import lombok.Builder;
import lombok.Getter;

import java.time.LocalDate;
import java.util.List;
import java.util.Objects;

@Getter
@Builder
public class TestExecution {

    private static final List<Object> CRITICAL_AND_BLOCKER = List.of(Priority.CRITICAL, Priority.BLOCKER);

    private final String cycleName;
    private final String issueKey; // common part of a test case
    private final String testSummary;
    private final String labels;
    private final TestPhase testPhase;
    private final Environment environment;
    private final Maison maison;
    private final Country country;
    private final String version;
    private final Integer orderId;
    private final Priority priority;
    private final ExecutionStatus executionStatus;
    private final String executedBy;
    private final LocalDate executedOn;
    private final String assignee;

    public boolean fromCountry(Country country) {
        return Objects.equals(this.country, country);
    }

    public boolean isIssueKey(String issueKey) {
        return Objects.equals(this.issueKey, issueKey);
    }

    public boolean isFrom(Maison maison) {
        return getMaison() == maison;
    }

    public boolean isFirstStep() {
        return this.orderId == null || this.orderId == 1;
    }

    public boolean isCriticalOrBlocker() {
        return CRITICAL_AND_BLOCKER.contains(this.getPriority());
    }
}
