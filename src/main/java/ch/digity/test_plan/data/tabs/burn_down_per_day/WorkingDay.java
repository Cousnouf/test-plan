package ch.digity.test_plan.data.tabs.burn_down_per_day;

import ch.digity.test_plan.data.test_execution.TestExecution;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;

public class WorkingDay {
    public static final List<DayOfWeek> WEEK_END = List.of(DayOfWeek.SATURDAY, DayOfWeek.SUNDAY);

    private final LocalDate localDate;
    private final List<TestExecution> testExecutions;

    public WorkingDay(LocalDate localDate, List<TestExecution> testExecutions) {
        this.localDate = localDate;
        this.testExecutions = testExecutions;
    }

    boolean isWeekDayOrHasBeenWorked() {
        return !testExecutions.isEmpty() ||
                !WEEK_END.contains(localDate.getDayOfWeek());
    }

    public LocalDate getLocalDate() {
        return localDate;
    }

    public List<TestExecution> getTestExecutions() {
        return testExecutions;
    }

    public static WorkingDay of(LocalDate localDate, Map<LocalDate, List<TestExecution>> groupedByExecutedOn) {
        return new WorkingDay(localDate, groupedByExecutedOn.getOrDefault(localDate, List.of()));
    }
}
