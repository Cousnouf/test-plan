package ch.digity.test_plan.data.enumeration

import spock.lang.Specification

class MaisonSpec extends Specification {

    def "should get header values"() {
        expect:
        Maison.toHeaderValues() == ["ALS", "BEM", "CAR", "IWC", "JLC", "MTB", "PAN", "PIA", "RDU", "VAC", "VCA"]
    }
}
