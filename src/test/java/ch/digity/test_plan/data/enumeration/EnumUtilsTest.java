package ch.digity.test_plan.data.enumeration;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class EnumUtilsTest {

    @Test
    void should_check_basic_needs() {
        TestPhase testPhase = EnumUtils.from("JLC Phoenix E2E_SIT_EU", TestPhase.values());

        assertEquals(TestPhase.SIT, testPhase);
    }

    @Test
    void should_check_mont_blanc() {
        Maison maison = EnumUtils.from("MTB Phoenix E2E_SIT_EU", Maison.values());

        assertEquals(Maison.MTB, maison);
    }
}