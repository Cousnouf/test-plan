package ch.digity.test_plan.data.chart.line;

import ch.digity.test_plan.data.chart.ChartDrawer;
import ch.digity.test_plan.data.chart.ChartPart;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xddf.usermodel.*;
import org.apache.poi.xddf.usermodel.chart.*;
import org.apache.poi.xssf.usermodel.XSSFChart;
import org.apache.poi.xssf.usermodel.XSSFSheet;

import java.util.List;

public class ChartLineDrawer extends ChartDrawer {

    private final XSSFSheet xssfSheet;
    private final int rowNumber;
    private final XSSFChart chart;
    private final XDDFCategoryDataSource dataSource;
    private final XDDFLineChartData data;

    public ChartLineDrawer(XSSFSheet xssfSheet, int rowNumber, XSSFChart chart, XDDFCategoryDataSource dataSource, XDDFLineChartData data) {
        this.xssfSheet = xssfSheet;
        this.rowNumber = rowNumber;
        this.chart = chart;
        this.dataSource = dataSource;
        this.data = data;
    }

    public void traceLines(List<ChartPart> chartParts) {
        chartParts.forEach(chartPart -> traceValues(xssfSheet, dataSource, data, chartPart.getColumnIndex(), chartPart.getLegend(), chartPart.getPresetColor()));
        chart.plot(data);
    }

    private void traceValues(XSSFSheet xssfSheet, XDDFCategoryDataSource dataSource, XDDFLineChartData data, int columnIndex, String legend, PresetColor presetColor) {
        XDDFNumericalDataSource<Double> numericalDataSource = XDDFDataSourcesFactory.fromNumericCellRange(xssfSheet, new CellRangeAddress(1, rowNumber, columnIndex, columnIndex));
        XDDFLineChartData.Series series = (XDDFLineChartData.Series) data.addSeries(dataSource, numericalDataSource);
        series.setTitle(legend, null);
        series.setSmooth(false);
        XDDFSolidFillProperties fill = new XDDFSolidFillProperties(XDDFColor.from(presetColor));
        XDDFLineProperties line = new XDDFLineProperties();
        line.setFillProperties(fill);
        XDDFShapeProperties properties = new XDDFShapeProperties();
        properties.setLineProperties(line);
        series.setShapeProperties(properties);
    }

    public static ChartLineDrawer of(String title, String leftAxisTitle, XSSFSheet xssfSheet, int rowNumber, int leftYPosition, int chartWidthColumn, int chartHeightColumn) {
        XSSFChart chart = ChartDrawer.getXssfChart(xssfSheet, leftYPosition, chartWidthColumn, chartHeightColumn);

        XDDFCategoryAxis bottomAxis = chart.createCategoryAxis(AxisPosition.BOTTOM);
        bottomAxis.setTitle(title);
        XDDFValueAxis leftAxis = chart.createValueAxis(AxisPosition.LEFT);
        leftAxis.setTitle(leftAxisTitle);
        leftAxis.setCrosses(AxisCrosses.AUTO_ZERO);

        leftAxis.setCrossBetween(AxisCrossBetween.BETWEEN);

        XDDFCategoryDataSource categoryDataSource = XDDFDataSourcesFactory.fromStringCellRange(xssfSheet, new CellRangeAddress(1, rowNumber, 0, 0));
        XDDFLineChartData data = (XDDFLineChartData) chart.createData(ChartTypes.LINE, bottomAxis, leftAxis);

        return new ChartLineDrawer(xssfSheet, rowNumber, chart, categoryDataSource, data);
    }
}
