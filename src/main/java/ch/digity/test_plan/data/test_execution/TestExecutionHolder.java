package ch.digity.test_plan.data.test_execution;

import ch.digity.test_plan.data.enumeration.Country;
import ch.digity.test_plan.data.enumeration.Maison;
import org.apache.commons.lang3.ObjectUtils;

import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

import static org.apache.commons.lang3.ObjectUtils.isEmpty;

public class TestExecutionHolder {
    private final List<TestExecution> testExecutions;

    public TestExecutionHolder(List<TestExecution> testExecutions) {
        this.testExecutions = testExecutions;
    }

    public List<TestExecution> getAll() {
        return testExecutions.stream()
                .filter(TestExecution::isFirstStep)
                .collect(Collectors.toList());
    }

    public List<TestExecution> getBlockersAndCritical() {
        return getAll().stream()
                .filter(TestExecution::isCriticalOrBlocker)
                .collect(Collectors.toList());
    }

    public <T> List<T> getDistinct(Function<TestExecution, T> mapper) {
        return getAll().stream()
                .map(mapper)
                .distinct()
                .sorted()
                .collect(Collectors.toList());
    }

    public <T> List<List<T>> getDistinctNonEmpty(List<Function<TestExecution, T>> mappers) {
        return getAll().stream()
                .map(testExecution -> getValues(testExecution, mappers))
                .filter(ObjectUtils::isNotEmpty)
                .distinct()
                .collect(Collectors.toList());
    }

    private <T> List<T> getValues(TestExecution testExecution, List<Function<TestExecution, T>> mappers) {
        if (mappers.stream().anyMatch(mapper -> isEmpty(mapper.apply(testExecution)))) {
            return List.of();
        }
        return mappers.stream()
                .map(mapper -> mapper.apply(testExecution))
                .collect(Collectors.toList());
    }

    public Optional<TestExecution> getFrom(String issueKey, Country country) {
        return getAll().stream()
                .filter(testExecution -> testExecution.fromCountry(country))
                .filter(testExecution -> testExecution.isIssueKey(issueKey))
                .findFirst();
    }

    public Optional<TestExecution> getFrom(String issueKey, Country country, Maison maison) {
        return getAll().stream()
                .filter(testExecution -> testExecution.isFrom(maison))
                .filter(testExecution -> testExecution.fromCountry(country))
                .filter(testExecution -> testExecution.isIssueKey(issueKey))
                .findFirst();
    }
}
