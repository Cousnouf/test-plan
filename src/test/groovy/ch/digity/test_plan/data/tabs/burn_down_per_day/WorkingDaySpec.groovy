package ch.digity.test_plan.data.tabs.burn_down_per_day

import ch.digity.test_plan.data.test_execution.TestExecution
import spock.lang.Specification

import java.time.LocalDate

class WorkingDaySpec extends Specification {

    def "should consider working day if week-end but with test executions"() {
        given:
        def workingDay = WorkingDay.of(LocalDate.of(2021, 10, 10), Map.of(LocalDate.of(2021, 10, 10), [Stub(TestExecution)]))

        expect:
        workingDay.isWeekDayOrHasBeenWorked()
    }
}
