package ch.digity.test_plan.data.enumeration

import spock.lang.Specification

class EnumUtilsSpec extends Specification {

    def "should decode properly generic enums"() {
        expect:
        EnumUtils.from(version, enumValues) == expected

        where:
        version                   | enumValues         | expected
        "JLC Phoenix E2E_SIT1_EU" | TestPhase.values() | TestPhase.SIT1
        "JLC Phoenix E2E_SIT1_JP" | TestPhase.values() | TestPhase.SIT1
    }
}
