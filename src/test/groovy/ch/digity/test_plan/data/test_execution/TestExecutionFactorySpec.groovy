package ch.digity.test_plan.data.test_execution

import ch.digity.test_plan.data.enumeration.*
import ch.digity.test_plan.data.export_row.ExportRow
import ch.digity.test_plan.data.test_execution.TestExecutionFactory
import spock.lang.Specification

class TestExecutionFactorySpec extends Specification {

    def "should parse correctly"() {
        given:
        ExportRow exportRow = Stub(ExportRow) {
            getVersion() >> input
            getPriority() >> Priority.BLOCKER
            getExecutedOn() >> null
        }

        when:
        def testExecution = TestExecutionFactory.from([exportRow])

        then:
        with(testExecution[0]) {
            testPhase == TestPhase.KUT2
            environment == Environment.VAL
            maison == Maison.CAR
            country == Country.CZECHIA
            priority == Priority.BLOCKER
        }

        where:
        input                                 | _
        "LOC EU Ph1 - KUT2 - VAL - CAR - CZ " | _
        "LOC EU Ph1 - KUT2 - VAL-CAR-CZ "     | _
        "LOC - KUT2 - VAL-CAR-CZ "            | _
        "B KUT2-VAL-CAR-CZ"                   | _
    }

    def "should not have same name between enums"() {
        given:
        def enumSet = new HashSet()
        enumSet.addAll(Maison.values().collect { it.toString() })
        enumSet.addAll(TestPhase.values().collect { it.toString() })
        enumSet.addAll(Environment.values().collect { it.toString() })
        enumSet.addAll(Country.values().collect { it.toString() })

        expect:
        def totalEnumSize = Maison.values().size() + TestPhase.values().size() + Environment.values().size() + Country.values().size()
        enumSet.size() == totalEnumSize
    }
}
