package ch.digity.test_plan.data.enumeration;

import java.util.List;
import java.util.stream.Collectors;

import static java.util.Arrays.stream;

public enum Maison {

    ALS,
    BEM,
    CAR,
    IWC,
    JLC,
    MTB,
    PAN,
    PIA,
    RDU,
    VAC,
    VCA;

    public static List<String> toHeaderValues() {
        return stream(values())
                .map(Enum::toString)
                .collect(Collectors.toList());
    }
}
