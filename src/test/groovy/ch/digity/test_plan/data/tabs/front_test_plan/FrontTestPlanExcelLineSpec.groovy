package ch.digity.test_plan.data.tabs.front_test_plan


import spock.lang.Specification

import static ch.digity.test_plan.data.enumeration.Country.BELGIUM
import static ch.digity.test_plan.data.enumeration.ExecutionStatus.*
import static ch.digity.test_plan.data.enumeration.Maison.*
import static ch.digity.test_plan.data.enumeration.Priority.CRITICAL
import static java.util.Map.of

class FrontTestPlanExcelLineSpec extends Specification {

    def "should create the excel line properly"() {
        when:
        def excelLine = FrontTestPlanExcelLine.of(
                "ik", "ts", CRITICAL, BELGIUM, of(IWC, FAIL, BEM, PASS, ALS, UNEXECUTED)
        )

        then: 'should provide value list with the execution status ordered by Maison names (ALS, BEM, IWC)'
        excelLine.toValueList().collect { it.stringValue } == ["ik", "ts", "CRITICAL", "BE", "UNEXECUTED", "PASS", "FAIL"]
    }
}
