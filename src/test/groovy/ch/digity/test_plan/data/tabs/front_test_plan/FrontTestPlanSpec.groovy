package ch.digity.test_plan.data.tabs.front_test_plan

import ch.digity.test_plan.data.test_execution.TestExecution
import ch.digity.test_plan.data.test_execution.TestExecutionHolder
import spock.lang.Specification

import java.util.function.Function

import static ch.digity.test_plan.data.enumeration.Country.AUSTRIA
import static ch.digity.test_plan.data.enumeration.Country.BELGIUM
import static ch.digity.test_plan.data.enumeration.ExecutionStatus.*
import static ch.digity.test_plan.data.enumeration.Maison.*

class FrontTestPlanSpec extends Specification {

    TestExecutionHolder holder = Stub()

    FrontTestPlan frontTestPlan

    void setup() {
        frontTestPlan = new FrontTestPlan(holder)
    }

    def "should get proper sheet name"() {
        expect:
        frontTestPlan.getSheetName() == "Front test plan"
    }

    def "should get the header"() {
        expect:
        frontTestPlan.getHeader().toValueList().collect { it.stringValue } == [
                "Front Issue Key", "Test Summary", "Priority", "Country", "ALS", "BEM", "CAR", "IWC", "JLC", "MTB", "PAN", "PIA", "RDU", "VAC", "VCA"
        ]
    }

    def "should produce ordered and consistent excel lines"() {
        given:
        holder.getDistinct(_ as Function) >> ["ik1", "ik2"] >> [AUSTRIA, BELGIUM]
        holder.getFrom("ik1", AUSTRIA) >> Optional.of(Stub(TestExecution) {
            getIssueKey() >> "ik1"
            getCountry() >> AUSTRIA
        })
        holder.getFrom("ik1", BELGIUM) >> Optional.of(Stub(TestExecution) {
            getIssueKey() >> "ik1"
            getCountry() >> BELGIUM
        })
        holder.getFrom("ik2", AUSTRIA) >> Optional.of(Stub(TestExecution) {
            getIssueKey() >> "ik2"
            getCountry() >> AUSTRIA
        })
        holder.getFrom("ik2", BELGIUM) >> Optional.of(Stub(TestExecution) {
            getIssueKey() >> "ik2"
            getCountry() >> BELGIUM
        })
        holder.getFrom("ik1", AUSTRIA, PAN) >> Optional.of(Stub(TestExecution) { getExecutionStatus() >> PASS })
        holder.getFrom("ik1", BELGIUM, BEM) >> Optional.of(Stub(TestExecution) { getExecutionStatus() >> WIP })
        holder.getFrom("ik2", AUSTRIA, CAR) >> Optional.of(Stub(TestExecution) { getExecutionStatus() >> FAIL })
        holder.getFrom("ik2", BELGIUM, VAC) >> Optional.of(Stub(TestExecution) { getExecutionStatus() >> BLOCKED })

        when:
        def excelLines = frontTestPlan.getExcelLines()

        then:
        excelLines.size() == 4
        excelLines[0].toValueList().collect { it.stringValue } == ["ik1", "", "BLOCKER", "AT", "NA", "NA", "NA", "NA", "NA", "NA", "PASS", "NA", "NA", "NA", "NA"]
        excelLines[1].toValueList().collect { it.stringValue } == ["ik1", "", "BLOCKER", "BE", "NA", "WIP", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA"]
        excelLines[2].toValueList().collect { it.stringValue } == ["ik2", "", "BLOCKER", "AT", "NA", "NA", "FAIL", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA"]
        excelLines[3].toValueList().collect { it.stringValue } == ["ik2", "", "BLOCKER", "BE", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "BLOCKED", "NA"]
    }
}
