package ch.digity.test_plan.data.enumeration;

import java.util.regex.Pattern;
import java.util.stream.Stream;

public class EnumUtils {

    private EnumUtils() {
    }

    public static final String NON_UNICODE_LETTER_NUMBER = "[^\\pL\\pN\\s]"; // pL unicode letter, pN unicode number // NOSONAR

    public static <T extends Enum<T>> T from(String version, T[] enumClass) {
        return Stream.of(enumClass)
                .filter(enumValue -> matchesAsWord(enumValue.name(), version))
                .findFirst()
                .orElse(null);
    }

    static boolean matchesAsWord(String enumName, String version) {
        return Pattern.compile("\\b" + enumName + "\\b")
                .matcher(version.replaceAll(NON_UNICODE_LETTER_NUMBER, " "))
                .find();
    }
}
