package ch.digity.test_plan.data.tabs.front_test_plan;

import ch.digity.test_plan.data.enumeration.Country;
import ch.digity.test_plan.data.enumeration.ExecutionStatus;
import ch.digity.test_plan.data.enumeration.Maison;
import ch.digity.test_plan.data.enumeration.Priority;
import ch.digity.test_plan.data.tabs.CellValueWrapper;
import ch.digity.test_plan.data.tabs.ExcelLine;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.TreeMap;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toUnmodifiableList;

public class FrontTestPlanExcelLine implements ExcelLine {

    private final String issueKey;
    private final String testSummary;
    private final Priority priority;
    private final Country country;
    private final List<ExecutionStatus> maisonExecutionStatuses;

    private FrontTestPlanExcelLine(String issueKey, String testSummary, Priority priority, Country country, List<ExecutionStatus> maisonExecutionStatuses) {
        this.issueKey = issueKey;
        this.testSummary = testSummary;
        this.priority = priority;
        this.country = country;
        this.maisonExecutionStatuses = maisonExecutionStatuses;
    }

    @Override
    public List<CellValueWrapper> toValueList() {
        List<String> values = new ArrayList<>(List.of(issueKey, testSummary, priority.toString(), String.valueOf(country)));
        values.addAll(maisonExecutionStatuses.stream().map(Objects::toString).collect(Collectors.toList()));
        return values.stream()
                .map(CellValueWrapper::new)
                .collect(Collectors.toList());
    }

    public static ExcelLine of(String issueKey, String testSummary, Priority priority, Country country, Map<Maison, ExecutionStatus> maisonExecutionStatusMap) {
        List<ExecutionStatus> orderedByMaisonExecutionStatuses = new TreeMap<>(maisonExecutionStatusMap).values().stream().collect(toUnmodifiableList());
        return new FrontTestPlanExcelLine(issueKey, testSummary, priority, country, orderedByMaisonExecutionStatuses);
    }
}
