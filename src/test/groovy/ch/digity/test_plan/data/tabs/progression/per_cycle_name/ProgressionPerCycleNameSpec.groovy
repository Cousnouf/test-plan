package ch.digity.test_plan.data.tabs.progression.per_cycle_name

import ch.digity.test_plan.data.enumeration.ExecutionStatus
import ch.digity.test_plan.data.enumeration.NumberFormat
import ch.digity.test_plan.data.tabs.progression.Progression
import ch.digity.test_plan.data.tabs.progression.ProgressionSpec
import ch.digity.test_plan.data.test_execution.TestExecution
import ch.digity.test_plan.data.test_execution.TestExecutionHolder

import java.util.function.Function

import static ch.digity.test_plan.data.enumeration.ExecutionStatus.FAIL
import static ch.digity.test_plan.data.enumeration.ExecutionStatus.PASS
import static org.apache.commons.lang3.ObjectUtils.defaultIfNull

class ProgressionPerCycleNameSpec extends ProgressionSpec {

    TestExecutionHolder testExecutionHolder = Stub()

    Progression progressionPerFeature

    void setup() {
        testExecutionHolder.getDistinctNonEmpty(_ as List<Function>) >> [["E2E"], ["Localization"]]
        progressionPerFeature = new Progression(testExecutionHolder, [asFunction(TestExecution::getCycleName)], "Cycle name", NumberFormat.PERCENTAGE)
    }

    def "should get proper sheet name"() {
        expect:
        progressionPerFeature.getSheetName() == "Progression per cycle name"
    }

    def "should get the header"() {
        expect:
        progressionPerFeature.getHeader().toValueList().collect { it.stringValue } == [
                "Cycle name", "PASS", "WIP", "FAIL", "BLOCKED", "UNEXECUTED", "Total"
        ]
    }

    def "should generate the line properly"() {
        given:
        testExecutionHolder.getAll() >> [te(FAIL, "E2E"), te(PASS, "E2E"), te(PASS, "E2E"), te(PASS, "E2E"), // E2E
                                         te(FAIL), te(), te(), te(), te(FAIL)] // localization

        when:
        def excelLines = progressionPerFeature.getExcelLines()

        then:
        excelLines.size() == 2
        excelLines[0].toValueList().collect {
            defaultIfNull(it.stringValue, it.numberValue)
        } == ["E2E", 3, 0.0, 1, 0.0, 0.0, 4.0]
        excelLines[1].toValueList().collect {
            defaultIfNull(it.stringValue, it.numberValue)
        } == ["Localization", 3, 0.0, 2, 0.0, 0.0, 5.0]
    }

    TestExecution te(ExecutionStatus executionStatus = PASS, cycleName = "Localization") {
        def testExecution = Stub(TestExecution)
        testExecution.getExecutionStatus() >> executionStatus
        testExecution.getCycleName() >> cycleName
        testExecution
    }
}
