package ch.digity.test_plan.data.enumeration;

import java.util.List;

public enum ExecutionStatus {

    NA,
    PASS,
    FAIL,
    WIP,
    BLOCKED,
    UNEXECUTED;

    public boolean isExecuted() {
        return List.of(PASS, FAIL, BLOCKED)
                .contains(this);
    }

    public static List<ExecutionStatus> forDisplay() {
        return List.of(PASS, WIP, FAIL, BLOCKED, UNEXECUTED);
    }
}
