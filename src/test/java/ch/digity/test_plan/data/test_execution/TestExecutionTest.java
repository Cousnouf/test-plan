package ch.digity.test_plan.data.test_execution;

import ch.digity.test_plan.data.enumeration.Country;
import ch.digity.test_plan.data.enumeration.Maison;
import ch.digity.test_plan.data.enumeration.Priority;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class TestExecutionTest {

    @Test
    @DisplayName("Should return true when the priority is critical")
    void isCriticalOrBlockerWhenPriorityIsCriticalThenReturnTrue() {
        TestExecution testExecution = TestExecution.builder().priority(Priority.CRITICAL).build();

        boolean result = testExecution.isCriticalOrBlocker();

        assertTrue(result);
    }

    @Test
    @DisplayName("Should return true when the priority is blocker")
    void isCriticalOrBlockerWhenPriorityIsBlockerThenReturnTrue() {
        TestExecution testExecution = TestExecution.builder().priority(Priority.BLOCKER).build();

        boolean result = testExecution.isCriticalOrBlocker();

        assertTrue(result);
    }

    @Test
    @DisplayName("Should return true when the issue key is the same")
    void isIssueKeyWhenTheIssueKeyIsTheSameThenReturnTrue() {
        TestExecution testExecution = TestExecution.builder().issueKey("ISSUE-1").build();

        boolean result = testExecution.isIssueKey("ISSUE-1");

        assertTrue(result);
    }

    @Test
    @DisplayName("Should return false when the issue key is not the same")
    void isIssueKeyWhenTheIssueKeyIsNotTheSameThenReturnFalse() {
        TestExecution testExecution = TestExecution.builder().issueKey("ISSUE-1").build();

        boolean result = testExecution.isIssueKey("ISSUE-2");

        assertFalse(result);
    }

    @Test
    @DisplayName("Should return true when the maison is the same")
    void isFromWhenMaisonIsTheSameThenReturnTrue() {
        TestExecution testExecution = TestExecution.builder().maison(Maison.ALS).build();

        boolean result = testExecution.isFrom(Maison.ALS);

        assertTrue(result);
    }

    @Test
    @DisplayName("Should return false when the maison is not the same")
    void isFromWhenMaisonIsNotTheSameThenReturnFalse() {
        TestExecution testExecution = TestExecution.builder().maison(Maison.ALS).build();

        boolean result = testExecution.isFrom(Maison.BEM);

        assertFalse(result);
    }

    @Test
    @DisplayName("Should return false when the country is not the same")
    void fromCountryWhenTheCountryIsNotTheSameThenReturnFalse() {
        TestExecution testExecution = TestExecution.builder().country(Country.FRANCE).build();

        boolean result = testExecution.fromCountry(Country.BELGIUM);

        assertFalse(result);
    }

    @Test
    @DisplayName("Should return true when the country is the same")
    void fromCountryWhenTheCountryIsTheSameThenReturnTrue() {
        TestExecution testExecution = TestExecution.builder().country(Country.FRANCE).build();

        boolean result = testExecution.fromCountry(Country.FRANCE);

        assertTrue(result);
    }

    @Test
    @DisplayName("Should return true when the orderid is null")
    void isFirstStepWhenOrderIdIsNull() {
        TestExecution testExecution = TestExecution.builder().orderId(null).build();

        assertTrue(testExecution.isFirstStep());
    }

    @Test
    @DisplayName("Should return true when the orderid is 1")
    void isFirstStepWhenOrderIdIs1() {
        TestExecution testExecution = TestExecution.builder().orderId(1).build();

        assertTrue(testExecution.isFirstStep());
    }
}