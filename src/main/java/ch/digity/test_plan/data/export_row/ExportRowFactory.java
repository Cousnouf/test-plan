package ch.digity.test_plan.data.export_row;

import com.opencsv.bean.CsvToBeanBuilder;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

public class ExportRowFactory {

    private ExportRowFactory() {
    }

    public static List<ExportRow> from(InputStream inputStream) {
        return new CsvToBeanBuilder<ExportRow>(new InputStreamReader(inputStream))
                .withType(ExportRow.class)
                .build()
                .parse();
    }
}
