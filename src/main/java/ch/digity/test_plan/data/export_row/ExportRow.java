package ch.digity.test_plan.data.export_row;

import ch.digity.test_plan.data.enumeration.ExecutionStatus;
import ch.digity.test_plan.data.enumeration.Priority;
import ch.digity.test_plan.data.enumeration.PriorityConverter;
import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvCustomBindByName;
import com.opencsv.bean.CsvDate;
import lombok.Getter;

import java.time.LocalDate;

@Getter
public class ExportRow {

    public static final String DATE_FORMAT = "yyyy-MM-dd";

    @CsvBindByName
    private long executionId;
    @CsvBindByName
    private String cycleName;
    @CsvBindByName(column = "issue key")
    private String issueKey;
    @CsvBindByName(column = "test summary")
    private String testSummary;
    @CsvBindByName
    private String labels;
    @CsvBindByName
    private String project;
    @CsvBindByName
    private String component;
    @CsvBindByName
    private String version;
    @CsvCustomBindByName(converter = PriorityConverter.class)
    private Priority priority;
    @CsvBindByName(column = "assigned to")
    private String assignedTo;
    @CsvBindByName(column = "executed by")
    private String executedBy;
    @CsvBindByName(column = "executed on")
    @CsvDate(value = DATE_FORMAT)
    private LocalDate executedOn;
    @CsvBindByName
    private ExecutionStatus executionStatus;
    @CsvBindByName
    private String comment;
    @CsvBindByName
    private String executionDefects;
    @CsvBindByName
    @CsvDate(value = DATE_FORMAT)
    private LocalDate creationDate;
    @CsvBindByName(column = "folder name")
    private String folderName;
    @CsvBindByName(column = "custom fields")
    private String customFields;
    @CsvBindByName
    private Long stepId;
    @CsvBindByName
    private Integer orderId;
    @CsvBindByName
    private String step;
    @CsvBindByName(column = "test data")
    private String testData;
    @CsvBindByName(column = "expected result")
    private String expectedResult;
    @CsvBindByName(column = "step result")
    private ExecutionStatus stepResult;
    @CsvBindByName
    private String comments;
    @CsvBindByName(column = "test step custom fields")
    private String testStepCustomFields;

    @Override
    public String toString() {
        return "ch.digity.test_plan.model.ExportRow{" +
                "executionId=" + executionId +
                ", cycleName='" + cycleName + '\'' +
                ", issueKey='" + issueKey + '\'' +
                ", testSummary='" + testSummary + '\'' +
                ", labels='" + labels + '\'' +
                ", project='" + project + '\'' +
                ", component='" + component + '\'' +
                ", version='" + version + '\'' +
                ", priority='" + priority + '\'' +
                ", assignedTo='" + assignedTo + '\'' +
                ", executedBy='" + executedBy + '\'' +
                ", executedOn='" + executedOn + '\'' +
                ", executionStatus=" + executionStatus +
                ", comment='" + comment + '\'' +
                ", executionDefects='" + executionDefects + '\'' +
                ", creationDate=" + creationDate +
                ", folderName='" + folderName + '\'' +
                ", customFields='" + customFields + '\'' +
                ", stepId=" + stepId +
                ", orderId=" + orderId +
                ", step='" + step + '\'' +
                ", testData='" + testData + '\'' +
                ", expectedResult='" + expectedResult + '\'' +
                ", stepResult=" + stepResult +
                ", comments='" + comments + '\'' +
                ", testStepCustomFields='" + testStepCustomFields + '\'' +
                '}';
    }
}
