package ch.digity.test_plan.data.tabs.burn_down_per_day;

import ch.digity.test_plan.data.enumeration.NumberFormat;
import ch.digity.test_plan.data.tabs.CellValueWrapper;
import ch.digity.test_plan.data.tabs.ExcelLine;

import java.time.LocalDate;
import java.util.List;

public class BurnDownPerDayExcelLine implements ExcelLine {

    private final LocalDate localDate;
    private final int remaining;
    private final double remainingAverage;
    private final int executed;
    private final double average;

    public BurnDownPerDayExcelLine(LocalDate localDate, int remaining, double remainingAverage, int executed, double average) {
        this.localDate = localDate;
        this.remaining = remaining;
        this.remainingAverage = remainingAverage;
        this.executed = executed;
        this.average = average;
    }

    public LocalDate getLocalDate() {
        return localDate;
    }

    @Override
    public List<CellValueWrapper> toValueList() {
        return List.of(
                new CellValueWrapper(remaining, NumberFormat.ABSOLUTE),
                new CellValueWrapper(remainingAverage, NumberFormat.ABSOLUTE),
                new CellValueWrapper(executed, NumberFormat.ABSOLUTE),
                new CellValueWrapper(average, NumberFormat.ABSOLUTE)
        );
    }
}
