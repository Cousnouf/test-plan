package ch.digity.test_plan.data.tabs;

import java.util.List;

public interface ExcelLine {

    List<CellValueWrapper> toValueList();

    default double getTotal() {
        return 0;
    }
}
