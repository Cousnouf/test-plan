package ch.digity.test_plan.data.chart;

import org.apache.poi.ss.usermodel.ClientAnchor;
import org.apache.poi.xddf.usermodel.chart.LegendPosition;
import org.apache.poi.xddf.usermodel.chart.XDDFChartLegend;
import org.apache.poi.xssf.usermodel.XSSFChart;
import org.apache.poi.xssf.usermodel.XSSFDrawing;
import org.apache.poi.xssf.usermodel.XSSFSheet;

public abstract class ChartDrawer {

    protected ChartDrawer() {
    }

    protected static XSSFChart getXssfChart(XSSFSheet xssfSheet, int leftYPosition, int chartWidthColumn, int chartHeightColumn) {
        XSSFDrawing drawing = xssfSheet.createDrawingPatriarch();
        ClientAnchor anchor = drawing.createAnchor(0, 0, 0, 0, 0, leftYPosition + 2, chartWidthColumn, leftYPosition + chartHeightColumn);
        XSSFChart chart = drawing.createChart(anchor);
        XDDFChartLegend legend = chart.getOrAddLegend();
        legend.setPosition(LegendPosition.RIGHT);
        return chart;
    }
}
